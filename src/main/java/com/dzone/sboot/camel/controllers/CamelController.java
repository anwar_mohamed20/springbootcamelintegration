package com.dzone.sboot.camel.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.dzone.sboot.camel.service.ConsumerService;
import com.dzone.sboot.camel.service.ProducerService;

@RestController
@RequestMapping("/api/v1")

public class CamelController {
	
	@Autowired
	ProducerService producerService;
	
	@Autowired
	ConsumerService consumerService;
	
	@RequestMapping(value = "/send")
	public void send( @RequestParam("msisdn") String msisdn, @RequestParam("msg") String msg ) {
		producerService.produceMsg(msisdn, msg);
	}
	
	@RequestMapping(value = "/receive")
	public void receive() {
		consumerService.receiveMsg();
	}
}
