package com.dzone.sboot.camel.service;

import org.apache.camel.CamelContext;
import org.apache.camel.ConsumerTemplate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ConsumerService {

	private Logger logger = Logger.getLogger(ConsumerService.class);

	@Autowired
	CamelContext camelContext;
	
	@Autowired
	ConsumerTemplate consumerTemplate;
	
	@Value("${smpp.ip}")
	private String ip;
	
	@Value("${smpp.port}")
	private String port;
	
	@Value("${smpp.user.name}")
	private String userName;
	
	@Value("${smpp.user.password}")
	private String password;

		public void receiveMsg(){	
		String msg=(String) consumerTemplate.receiveBody("smpp://"+userName+"@"+ip+":"+port+"?password="+password+"&enquireLinkTimer=45000&transactionTimer=5000&serviceType=CMT&typeOfNumber=1&numberingPlanIndicator=0&systemType=smpp.dtest");	 
	    logger.info("mesage received " +msg);
	}
}
