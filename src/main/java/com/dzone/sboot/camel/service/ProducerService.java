package com.dzone.sboot.camel.service;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ProducerService {

	private Logger logger = Logger.getLogger(ProducerService.class);
	
	@Autowired
	CamelContext camelContext;
	
	@Autowired
	ProducerTemplate producerTemplate;
	
	@Value("${smpp.ip}")
	private String ip;
	
	@Value("${smpp.port}")
	private String port;
	
	@Value("${smpp.user.name}")
	private String userName;
	
	@Value("${smpp.user.password}")
	private String password;
	
	public void produceMsg(String msisdn, String msg){	
		producerTemplate.sendBody("smpp://"+userName+"@"+ip+":"+port+"?password="+password+"&enquireLinkTimer=45000&transactionTimer=5000&serviceType=CMT&destAddr="+msisdn+"&typeOfNumber=1&numberingPlanIndicator=0&systemType=smpp.dtest&sourceAddr=ISWTest", msg);	 
	    logger.info("mesage sent to " +msisdn +"  " +msg);
	}
}
